import face_recognition as fr
import os
import cv2
import numpy as np
from imutils import paths

MODEL = 'cnn' 
TOLERANCE = 0.45

# get captured face encodings and names
def get_camera_face_encoding(file = None):
    """funcion gets encoding/embedding from an image1"""
    known_names = []
    image = fr.load_image_file(file) 
    name = file.split('/')[-1]
    locations = fr.face_locations(image, model=MODEL)
    if len(locations) == 0:
        return []
    try:
        camera_encoding = fr.face_encodings(image, locations)
    except IndexError:
        print('The face was not captured rightly, retake the image') 

    known_names.append(name)

    return known_names, camera_encoding



def get_bvn_encoding(KNOWN_FACES_DIR):
    """funcion gets encoding/embedding from an image1"""
    
    # Now let's loop over a folder of faces we want to label
    for filename in os.listdir(KNOWN_FACES_DIR):

        # Load image
        # print(f'Filename {filename}', end='')
        image = fr.load_image_file(f'{KNOWN_FACES_DIR}/{filename}')

        # This time we first grab face locations - we'll need them to draw boxes
        locations = fr.face_locations(image, model=MODEL)

        # # Now since we know loctions, we can pass them to face_encodings as second argument
        # # Without that it will search for faces once again slowing down whole process
        encodings = fr.face_encodings(image, locations)
        # # We passed our image through face_locations and face_encodings, so we can modify it
        # # First we need to convert it from RGB to BGR as we are going to work with cv2
        return encodings


def compare_face(camera_encoding, bvn_encoding):
    """function compares image1 and image2 and produces a match result 
        and distance between the images"""

    for face_encoding in camera_encoding:
        results = fr.compare_faces(bvn_encoding, face_encoding, TOLERANCE)
        distance = fr.api.face_distance(bvn_encoding, face_encoding).tolist() 
        image_distance = round(distance[0],3)
        verification = { 'verify' : bool(results[0]),
                         'distance': image_distance
              
                      }

    return verification



# Returns (R, G, B) from name
def name_to_color(name):
    # Take 3 first letters, tolower()
    # lowercased character ord() value rage is 97 to 122, substract 97, multiply by 8
    color = [(ord(c.lower())-97)*8 for c in name[:3]]
    return color





print('Processing unknown faces...')
# Now let's loop over a folder of faces we want to label
for filename in os.listdir(FOLDER_KNOWN_FACES_BVN):

    # Load image
    print(f'Filename {filename}', end='')
    image = fr.load_image_file(f'{FOLDER_KNOWN_FACES_BVN}/{filename}')

    # This time we first grab face locations - we'll need them to draw boxes
    locations = fr.face_locations(image, model=MODEL)

    # Now since we know loctions, we can pass them to face_encodings as second argument
    # Without that it will search for faces once again slowing down whole process
    encodings = fr.face_encodings(image, locations)

    # We passed our image through face_locations and face_encodings, so we can modify it
    # First we need to convert it from RGB to BGR as we are going to work with cv2
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    # But this time we assume that there might be more faces in an image - we can find faces of dirrerent people
    print(f', found {len(encodings)} face(s)')
    for face_encoding, face_location in zip(encodings, locations):

        # We use compare_faces (but might use face_distance as well)
        # Returns array of True/False values in order of passed known_faces
        results = fr.compare_faces(camera_face_encoding, face_encoding, TOLERANCE)
        distance = fr.api.face_distance(camera_face_encoding, face_encoding).tolist()[0]
        distance = round(distance,2)
        print(results[0], distance)
        
    #     # Since order is being preserved, we check if any face was found then grab index
    #     # then label (name) of first matching known face withing a tolerance
    #     match = None
    #     if True in results:  # If at least one is true, get a name of first of found labels
    #         match = known_names[results.index(True)]
    #         # print(f' - {match} from {results}')
    #          # Each location contains positions in order: top, right, bottom, left
    #         top_left = (face_location[3], face_location[0])
    #         bottom_right = (face_location[1], face_location[2])
    #         print(match)

    #         # Get color by name using our fancy function
    #         color = name_to_color(match)
    #         cv2.rectangle(image, top_left, bottom_right, color, FRAME_THICKNESS)

    #         # Now we need smaller, filled grame below for a name
    #         # This time we use bottom in both corners - to start from bottom and move 50 pixels down
    #         top_left = (face_location[3], face_location[2])
    #         bottom_right = (face_location[1], face_location[2] + 22)

    #         # Paint frame
    #         cv2.rectangle(image, top_left, bottom_right, color, cv2.FILLED)
    #         # draw the predicted face name on the image
    #         text = "{}: {}%".format(match, distance)
    #         # Wite a name
    #         cv2.putText(image, text, (face_location[3] + 10, face_location[2] + 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 200, 200), FONT_THICKNESS)

    # # Show image
    # cv2.imshow(filename, image)
    # cv2.waitKey(0)
    # cv2.destroyWindow(filename)


    

