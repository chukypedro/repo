#python3 -m flask run
from flask import Flask
import os
import markdown
app = Flask(__name__)

@app.route('/')
def index():
    """Represent some documention"""

    #Open the README file
    with open(os.path.dirname(app.root_path) + '/README.md', 'r') as markdown_file:
        content = markdown_file()
        return markdown.markdown(content)