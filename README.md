# Face Verification Service
Build a REST API for face verication

## Usage

All responses will have the form

```
{
    "data": "Mixed type holding the content of the response",
    "message": "Description of what happened"
}

```

Subsequent response definitions will only detail the expected value of the `data field`

**API Endpoints:**
* face_match : return if two images are matched for the same person.
* face_rec : return a matched person's ID, name, and optional face details (face location and facial features)

# Installation
Install [face_recognition](https://github.com/ageitgey/face_recognition) together with [dlib](http://dlib.net/) first.
Then run: pip install -r requirements.txt

# List all devices

## Definition

`Get/face_match`
## Response
* `200 OK` on success

```
[
    {
        "identifier": "floor-lamp",
        "name": "Floor Lamp",
        "device_type": "switch",
        "controller_gateway": "192.1.68.0.2"
    },
    {
        "identifier": "samsung-tv",
        "name": "Living Room TV",
        "device_type": "tv",
        "controller_gateway": "192.168.0.9"
    }
]
```

# Getting a Face verified

## Definition

`POST / face_match`

Arguments

* name
* url to face 1
* url to face 2
* identifier


## Response

* `201 Created on success`

```
{
    "identifier": "floor-lamp",
    "name": "Floor Lamp",
    "device_type": "switch",
    "controller_gateway": "192.1.68.0.2"
}

```


# How to Run
## Prerequisites
Prepare some known faces as a database for face_rec API in sample_images folder, and modify known_faces in face_util.py accordingly.
```
# Each face is tuple of (Name,sample image)    
known_faces = [('Obama','sample_images/obama.jpg'), 
               ('Peter','sample_images/peter.jpg'),
              ]
```
## Run API Server
python flask_server.py


## Run API client - Web
Simply open a web browser and enter:

http://127.0.0.1:5001/face_rec

http://127.0.0.1:5001/face_match

and upload image files.

## Run API client - Python
python demo_client.py 


